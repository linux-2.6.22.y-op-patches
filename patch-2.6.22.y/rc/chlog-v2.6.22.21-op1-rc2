commit 5651fadddd81ecbab1e4fa33878cc8d0b1ddae6c
Author: Oliver Pinter <oliver.pntr@gmail.com>
Date:   Sat Mar 29 16:20:25 2008 +0100

    v2.6.22.21-op1-rc2
    
    Signed-off-by: Oliver Pinter <oliver.pntr@gmail.com>

commit 1ee0bb06edf100344d5b940f58439575863a6187
Author: andrew.patterson@hp.com <andrew.patterson@hp.com>
Date:   Sat Mar 29 16:18:53 2008 +0100

    MCA when shutting down tulip quad-NIC
    
    Shutting down the network causes an MCA because of an IO TLB error when
    a DEC quad 10/100 card is in any slot.  This problem was originally seen
    on an HP rx4640.
    
    Acked-by: Olaf Kirch <okir@suse.de>
    Signed-off-by: Oliver Pinter <oliver.pntr@gmail.com>
    
     drivers/net/tulip/tulip_core.c |    4 ++++
     1 file changed, 4 insertions(+)

commit b362b7edb1a3b9ba69eecb0cbd95fd4c4ba96994
Author: Joe Korty <joe.korty@ccur.com>
Date:   Wed Mar 5 15:04:59 2008 -0800

    slab: NUMA slab allocator migration bugfix
    
    NUMA slab allocator cpu migration bugfix
    
    The NUMA slab allocator (specifically, cache_alloc_refill)
    is not refreshing its local copies of what cpu and what
    numa node it is on, when it drops and reacquires the irq
    block that it inherited from its caller.  As a result
    those values become invalid if an attempt to migrate the
    process to another numa node occured while the irq block
    had been dropped.
    
    The solution is to make cache_alloc_refill reload these
    variables whenever it drops and reacquires the irq block.
    
    The error is very difficult to hit.  When it does occur,
    one gets the following oops + stack traceback bits in
    check_spinlock_acquired:
    
    	kernel BUG at mm/slab.c:2417
    	cache_alloc_refill+0xe6
    	kmem_cache_alloc+0xd0
    	...
    
    This patch was developed against 2.6.23, ported to and
    compiled-tested only against 2.6.25-rc4.
    
    Signed-off-by: Joe Korty <joe.korty@ccur.com>
    Signed-off-by: Christoph Lameter <clameter@sgi.com>
    Signed-off-by: Oliver Pinter <oliver.pntr@gmail.com>

commit 078288fc83e496c7965fa10b694353125c349ef7
Author: NeilBrown <neilb@suse.de>
Date:   Wed Feb 6 01:40:00 2008 -0800

    md: fix an occasional deadlock in raid5
    
    raid5's 'make_request' function calls generic_make_request on underlying
    devices and if we run out of stripe heads, it could end up waiting for one of
    those requests to complete.  This is bad as recursive calls to
    generic_make_request go on a queue and are not even attempted until
    make_request completes.
    
    So: don't make any generic_make_request calls in raid5 make_request until all
    waiting has been done.  We do this by simply setting STRIPE_HANDLE instead of
    calling handle_stripe().
    
    If we need more stripe_heads, raid5d will get called to process the pending
    stripe_heads which will call generic_make_request from a
    
    This change by itself causes a performance hit.  So add a change so that
    raid5_activate_delayed is only called at unplug time, never in raid5.  This
    seems to bring back the performance numbers.  Calling it in raid5d was
    sometimes too soon...
    
    Neil said:
    
      How about we queue it for 2.6.25-rc1 and then about when -rc2 comes out,
      we queue it for 2.6.24.y?
    
    Acked-by: Dan Williams <dan.j.williams@intel.com>
    Signed-off-by: Neil Brown <neilb@suse.de>
    Tested-by: dean gaudet <dean@arctic.org>
    Cc: <stable@kernel.org>
    Signed-off-by: Andrew Morton <akpm@linux-foundation.org>
    Signed-off-by: Linus Torvalds <torvalds@linux-foundation.org>
    Signed-off-by: Oliver Pinter <oliver.pntr@gmail.com>

commit 70a7cd1e8fbe736b854f39efd98528b7dfd64ee1
Author: Suresh Jayaraman <sjayaraman@suse.de>
Date:   Sat Mar 29 16:18:51 2008 +0100

    Fix sign mount option and sign proc config setting
    
    Patch mainline: 2.6.23-rc1
    
    Backported the fix (2.6.23-rc1) from Steve French. The original patch removes
    few commented functions (which are not required) as part of this fix, backport
    also does the same to retain compatibility.
    
    We were checking the wrong (old) global variable to determine
    whether to override server and force signing on the SMB
    connection.
    
    Acked-by: Dave Kleikamp <shaggy@austin.ibm.com>
    Signed-off-by: Steve French <sfrench@us.ibm.com>
    Signed-off-by: Suresh Jayaraman <sjayaraman@suse.de>
    Signed-off-by: Oliver Pinter <oliver.pntr@gmail.com>

commit 982b92285ec9f45e1658c95376f9e1a47ba47090
Author: Steve French <sfrench@us.ibm.com>
Date:   Tue Nov 13 22:41:37 2007 +0000

    Fix buffer overflow if server sends corrupt response to small
    
    [CIFS] Fix buffer overflow if server sends corrupt response to small
    request
    
    In SendReceive() function in transport.c - it memcpy's
    message payload into a buffer passed via out_buf param. The function
    assumes that all buffers are of size (CIFSMaxBufSize +
    MAX_CIFS_HDR_SIZE) , unfortunately it is also called with smaller
    (MAX_CIFS_SMALL_BUFFER_SIZE) buffers.  There are eight callers
    (SMB worker functions) which are primarily affected by this change:
    
    TreeDisconnect, uLogoff, Close, findClose, SetFileSize, SetFileTimes,
    Lock and PosixLock
    
    CC: Dave Kleikamp <shaggy@austin.ibm.com>
    CC: Przemyslaw Wegrzyn <czajnik@czajsoft.pl>
    Acked-by: Jeff Layton <jlayton@redhat.com>
    Signed-off-by: Steve French <sfrench@us.ibm.com>
    Acked-by: Jeff Mahoney <jeffm@suse.com>
    Signed-off-by: Oliver Pinter <oliver.pntr@gmail.com>

commit 54ddb8eee828607fb70a553b9473fe671fc2c87e
Author: Oliver Pinter <oliver.pntr@gmail.com>
Date:   Tue Feb 26 17:06:12 2008 +0100

    linux v2.6.22.20-op1-rc1
    
    Signed-off-by: Oliver Pinter <oliver.pntr@gmail.com>

commit dbd68c840f69fe6721fb609d0eb8a5d548e76a99
Author: Kurt Garloff <garloff@suse.de>
Date:   Mon Feb 18 21:16:27 2008 +0100

    make (low) swappiness safer to use
    
    The patch titled
         make swappiness safer to use
    has been added to the -mm tree.  Its filename is
         make-swappiness-safer-to-use.patch
    
    See http://www.zip.com.au/~akpm/linux/patches/stuff/added-to-mm.txt to find
    out what to do about this
    
    #Subject: make swappiness safer to use
    #From: Andrea Arcangeli <andrea@suse.de>
    
    Swappiness isn't a safe sysctl.  Setting it to 0 for example can hang a
    system.  That's a corner case but even setting it to 10 or lower can waste
    enormous amounts of cpu without making much progress.  We've customers who
    wants to use swappiness but they can't because of the current
    implementation (if you change it so the system stops swapping it really
    stops swapping and nothing works sane anymore if you really had to swap
    something to make progress).
    
    This patch from Kurt Garloff makes swappiness safer to use (no more huge
    cpu usage or hangs with low swappiness values).
    
    I think the prev_priority can also be nuked since it wastes 4 bytes per
    zone (that would be an incremental patch but I wait the nr_scan_[in]active
    to be nuked first for similar reasons).  Clearly somebody at some point
    noticed how broken that thing was and they had to add min(priority,
    prev_priority) to give it some reliability, but they didn't go the last
    mile to nuke prev_priority too.  Calculating distress only in function of
    not-racy priority is correct and sure more than enough without having to
    add randomness into the equation.
    
    Patch is tested on older kernels but it compiles and it's quite simple
    so...
    
    Overall I'm not very satisified by the swappiness tweak, since it doesn't
    rally do anything with the dirty pagecache that may be inactive.  We need
    another kind of tweak that controls the inactive scan and tunes the
    can_writepage feature (not yet in mainline despite having submitted it a
    few times), not only the active one.  That new tweak will tell the kernel
    how hard to scan the inactive list for pure clean pagecache (something the
    mainline kernel isn't capable of yet).  We already have that feature
    working in all our enterprise kernels with the default reasonable tune, or
    they can't even run a readonly backup with tar without triggering huge
    write I/O.  I think it should be available also in mainline later.
    
    Cc: Nick Piggin <npiggin@suse.de>
    Signed-off-by: Kurt Garloff <garloff@suse.de>
    Signed-off-by: Andrea Arcangeli <andrea@suse.de>
    Signed-off-by: Fengguang Wu <wfg@mail.ustc.edu.cn>
    Signed-off-by: Andrew Morton <akpm@linux-foundation.org>
    Signed-off-by: Oliver Pinter <oliver.pntr@gmail.com>

commit eef55466b00265cfe843ae58edd608a29ad0fc5e
Author: David Howells <dhowells@redhat.com>
Date:   Thu Feb 21 16:12:45 2008 +0000

    MM: Fix macro argument substitution in PageHead() and PageTail()
    
    Fix macro argument substitution in PageHead() and PageTail() - 'page' should
    have brackets surrounding it (commit 6d7779538f765963ced45a3fa4bed7ba8d2c277d).
    
    Signed-off-by: David Howells <dhowells@redhat.com>
    Signed-off-by: Linus Torvalds <torvalds@linux-foundation.org>
    Signed-off-by: Oliver Pinter <oliver.pntr@gmail.com>

commit 2cd082a9a5860afa64b032fc6df230e25d913289
Author: Thomas Gleixner <tglx@linutronix.de>
Date:   Wed Feb 20 00:29:02 2008 +0100

    genirq: do not leave interupts enabled on free_irq
    
    commit 89d694b9dbe769ca1004e01db0ca43964806a611
    
    The default_disable() function was changed in commit:
    
     76d2160147f43f982dfe881404cfde9fd0a9da21
     genirq: do not mask interrupts by default
    
    It removed the mask function in favour of the default delayed
    interrupt disabling. Unfortunately this also broke the shutdown in
    free_irq() when the last handler is removed from the interrupt for
    those architectures which rely on the default implementations. Now we
    can end up with a enabled interrupt line after the last handler was
    removed, which can result in spurious interrupts.
    
    Fix this by adding a default_shutdown function, which is only
    installed, when the irqchip implementation does provide neither a
    shutdown nor a disable function.
    
    Pointed-out-by: Michael Hennerich <Michael.Hennerich@analog.com>
    Signed-off-by: Thomas Gleixner <tglx@linutronix.de>
    Acked-by: Ingo Molnar <mingo@elte.hu>
    Tested-by: Michael Hennerich <Michael.Hennerich@analog.com>
    Signed-off-by: Greg Kroah-Hartman <gregkh@suse.de>
    Signed-off-by: Oliver Pinter <oliver.pntr@gmail.com>

commit c3bec7bc563415560734d190f33083716bb1cb68
Author: Ingo Molnar <mingo@elte.hu>
Date:   Fri Feb 15 20:58:22 2008 +0100

    x86_64: CPA, fix cache attribute inconsistency bug, v2.6.22 backport
    
    fix CPA cache attribute bug in v2.6.2[234]. When phys_base is nonzero
    (when CONFIG_RELOCATABLE=y) then change_page_attr_addr() miscalculates
    the secondary alias address by -14 MB (depending on the configured
    offset).
    
    The default 64-bit kernels of Fedora and Ubuntu are affected:
    
       $ grep RELOCA /boot/config-2.6.23.9-85.fc8
         CONFIG_RELOCATABLE=y
    
       $ grep RELOC /boot/config-2.6.22-14-generic
         CONFIG_RELOCATABLE=y
    
    and probably on many other distros as well.
    
    the bug affects all pages in the first 40 MB of physical RAM that
    are allocated by some subsystem that does ioremap_nocache() on them:
    
           if (__pa(address) < KERNEL_TEXT_SIZE) {
    
    Hence we might leave page table entries with inconsistent cache
    attributes around (pages mapped at both UnCacheable and Write-Back),
    and we can also set the wrong kernel text pages to UnCacheable.
    
    the effects of this bug can be random slowdowns and other misbehavior.
    If for example AGP allocates its aperture pages into the first 40 MB
    of physical RAM, then the -14 MB bug might mark random kernel texto
    pages as uncacheable, slowing down a random portion of the 64-bit
    kernel until the AGP driver is unloaded.
    
    Signed-off-by: Ingo Molnar <mingo@elte.hu>
    Acked-by: Thomas Gleixner <tglx@linutronix.de>
    Signed-off-by: Oliver Pinter <oliver.pntr@gmail.com>
